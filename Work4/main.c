#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>

double c = 1;
double h = 0.1;
double tau = 0.05;
double T = 4;
double left = 0;
double right = 10;

int NUM_THREADS = 2;
sem_t sem[4]; // NUM_THREADS * 2

double solution(double x) {
    if (x < 0) return 0;
    if (x > 2) return 0;
    return x*(2-x);
}

double exactSolution(double x, double t) {
    return solution(x - c*t);
}

struct Data {
   double *u;
   int  amount_data;
   int  start;
   int  num_thread;
};

void *CalcThread(void * arr) {
    struct Data *data = (struct Data *)arr;

    double *u_new = (double *)malloc(sizeof(double) * data->amount_data);
    int i, start = data->start;
    double cur_t;

    for (cur_t = tau; cur_t <= T; cur_t += tau) {
        if (data->num_thread == 0) {
            u_new[0] = 0;
        }

        if (data->num_thread != 0) {
            sem_wait(&sem[2*(data->num_thread-1)]);
                // Ждем пока левый сосед посчитает правое значение
                u_new[0] = (-c/h * (data->u[data->start] - data->u[data->start-1])) * tau + data->u[data->start];
                // Разрешаем левому соседу обновить массив u (я использовал значение левого)
            sem_post(&sem[2*data->num_thread + 1]);
        }

        for (i = data->start + 1; i < data->start + data->amount_data; ++i) {
            u_new[i-data->start] = (-c/h * (data->u[i] - data->u[i-1])) * tau + data->u[i];
        }

        for (i = data->start; i < data->start + data->amount_data - 1; ++i) {
            data->u[i] = u_new[i - data->start];
        }

        if (data->num_thread != NUM_THREADS-1) sem_wait(&sem[2*(data->num_thread+1)+1]);
            // Ждем пока правый сосед использует наше правое значение в u
            data->u[data->start + data->amount_data - 1] = u_new[data->amount_data - 1];
            // Разрешаем правому соседу вычислять свое первое значние (я досчитал свое правое)
        if (data->num_thread != NUM_THREADS-1) sem_post(&sem[2*data->num_thread]);
    }

    pthread_exit(NULL);
}

double x_coor(int i) {
    return left + i * h;
}

int main (int argc, char *argv[]) {
    int nPoints = (int)((right - left) / h); // Должно быть целым числом

    // Разделим данные между процессами максимально поровну
    // Номера процессов 0..(separator-1) получат amount_process_data+1
    // Номера процессов separator..(NUM_THREADS-1) получат amount_process_data
    int amount_process = NUM_THREADS;
    int amount_process_data = 0;
    int separator;
    int tail;
    int num_data = (int)nPoints;
    do {
        tail = num_data % amount_process;
        amount_process_data += num_data / amount_process;
        num_data = tail;
    } while(tail >= amount_process);
    separator = tail;

    //double u[nPoints]; // Индексом является координата
    double *u = (double *)malloc(sizeof(double) * nPoints);
    int i, j;

    //double cur_t = 0;
    for (i = 0; i < nPoints; ++i) {
        u[i] = solution(x_coor(i));
    }

    pthread_t threads[NUM_THREADS];
    int rc, start = 0;

    for (i = 0; i < 2*NUM_THREADS; ++i) {
        if (i % 2 == 0) {
            sem_init(&sem[i], 0, 1); //
        } else {
            sem_init(&sem[i], 0, 0); //
        }
    }

    struct Data *threadData = (struct Data *)malloc(sizeof(struct Data)*NUM_THREADS);

        for (i = 0; i < NUM_THREADS; ++i) {
            if (i < separator) {
                num_data = amount_process_data + 1;
            } else {
                num_data = amount_process_data;
            }

            threadData[i].amount_data = num_data;
            threadData[i].num_thread = i;
            threadData[i].start = start;
            threadData[i].u = u;
            start += num_data;

            rc = pthread_create(&threads[i], NULL, CalcThread, &(threadData[i]));
            if (rc) {
                free(threadData);
                printf("ERROR; return code from pthread_create() is %d\n", rc);
                exit(-1);
            }
        }

    for (i = 0; i < NUM_THREADS; ++i) {
        pthread_join(threads[i], NULL);
    }

    freopen("1.txt", "w", stdout);

    for (i = 0; i < nPoints; ++i) {
        printf("%d : %f \n", i, u[i]);
    }

    free(threadData);

    for (i = 0; i < 2*NUM_THREADS; ++i) {
        sem_destroy(&sem[i]);
    }

    // Exact solution
    printf("\n\n");
    for (i = 0; i < nPoints; ++i) {
        printf("%d : %f \n", i, exactSolution(x_coor(i), T));
    }
}
