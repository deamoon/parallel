// https://parallel.ru/vvv/mpi.html
// module add openmpi-x86_64
// mpicc 1.c
// mpiexec -np 5 ./a.out 101

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <mpi.h>

#define MAIN_PROCESS 0
#define MSG_TAG 1

#define TYPE_DATA int
#define MPI_TYPE_DATA MPI_INT

#define TYPE_SUM_DATA long long int
#define MPI_TYPE_SUM_DATA MPI_LONG_LONG_INT
#define SUM_DATA_FORMAT_STRING "%lld"

int main(int argc, char *argv[]) {	
    int i;
    int amount_process;
    int rank_process;
    int amount_data = atoi(argv[argc - 1]); // Последний аргумент размер

    MPI_Status Status;
    MPI_Init(&argc, &argv);
    
    MPI_Comm_size(MPI_COMM_WORLD, &amount_process);
    if (amount_process <= 1) {
        MPI_Finalize();
        exit(1);
    }
    int amount_sum_process = amount_process - 1;

    // Разделим данные между процессами максимально поровну
    // Процессы 1..separator получат amount_process_data+1
    // Процессы separator+1..amount_sum_process получат amount_process_data
    int amount_process_data = 0;
    int separator;
    int tail;
    int num_data = amount_data;
    do {
        tail = num_data % amount_sum_process;
        amount_process_data += num_data / amount_sum_process;
        num_data = tail;
    } while(tail >= amount_sum_process);
    separator = tail;
    
    MPI_Comm_rank(MPI_COMM_WORLD, &rank_process);
	
	if (rank_process == MAIN_PROCESS) {
        TYPE_DATA array[amount_data];
		for (i = 0; i < amount_data; ++i) {
			array[i] = i;
		}
        for (i = 1; i <= separator; ++i) {
            MPI_Send(&array[(amount_process_data+1)*(i-1)], amount_process_data+1, MPI_TYPE_DATA, 
                     i, MSG_TAG, MPI_COMM_WORLD) ;
        }
        int shift = (amount_process_data+1) * (separator);
        for (i = separator+1; i <= amount_sum_process; ++i) {
            MPI_Send(&array[shift + (amount_process_data)*(i-separator-1)], amount_process_data, MPI_TYPE_DATA, 
                     i, MSG_TAG, MPI_COMM_WORLD) ;
        }
        
        TYPE_SUM_DATA sum = 0;
        TYPE_SUM_DATA bufer;
        for (i = 1; i <= amount_sum_process; ++i) {
            MPI_Recv(&bufer, 1, MPI_TYPE_SUM_DATA, i, MSG_TAG, MPI_COMM_WORLD, &Status);
            sum += bufer;
        }

        printf("sum = "SUM_DATA_FORMAT_STRING"\n", sum);

        TYPE_SUM_DATA ideal_sum = 0;
        for (i = 0; i < amount_data; ++i) {
            ideal_sum += array[i];
        }
        printf("ideal_sum = "SUM_DATA_FORMAT_STRING"\n", ideal_sum);

        assert(ideal_sum == sum);
	}

	if (rank_process != MAIN_PROCESS) {
        if (rank_process <= separator) {
            ++amount_process_data;
        } 
        TYPE_DATA buffer[amount_process_data];
		TYPE_SUM_DATA sum = 0;

        MPI_Recv(buffer, amount_process_data, MPI_TYPE_DATA, MAIN_PROCESS, MSG_TAG, MPI_COMM_WORLD, &Status);
		for (i = 0; i < amount_process_data; ++i) {
			sum += buffer[i];
		}
        printf("Process rank = %d, sum = "SUM_DATA_FORMAT_STRING"\n", rank_process, sum);
		MPI_Send(&sum, 1, MPI_TYPE_SUM_DATA, MAIN_PROCESS, MSG_TAG, MPI_COMM_WORLD);
	}

	MPI_Finalize();
	return 0;
}
