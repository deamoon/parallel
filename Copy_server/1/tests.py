import unittest
from subprocess import *

compile_str = "mpiexec -np %d sum_array %d"

def check_program(num_proc, num_data):
	check_call(compile_str % (num_proc, num_data), stdout=PIPE, stderr=PIPE, shell=True)

class TestSumArray(unittest.TestCase):
	
	def testSimpleSumArray(self):
		check_program(10, 100)
		check_program(12, 158)
		check_program(34, 13258)
		check_program(12, 158)
		check_program(13, 12358)
		check_program(23, 1584214)

	def testDifferentSizeData(self):
		for i in range(5):
			check_program(10, i)	
		
if __name__ == "__main__":
	unittest.main()