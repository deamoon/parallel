#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>

double c = 1;
double h = 0.1;
double tau = 0.05;
double T = 4;
double left = 0;
double right = 10;

double solution(double x) {
    if (x < 0) return 0;
    if (x > 2) return 0;
    return x*(2-x);
}

double exactSolution(double x, double t) {
    return solution(x - c*t);
}

sem_t sem;

void *CalcThread(void * arr) {
    double *ar = (double *)arr;

    free(ar);

    sem_wait(&sem);
        //resultIntagrate += threadResult;
    sem_post(&sem);

    pthread_exit(NULL);
}

double x_coor(int i) {
    return left + i * h;
}

int main (int argc, char *argv[]) {
    int nPoints = (int)((right - left) / h); // Должно быть целым числом
    int NUM_THREADS = 10;

    // Разделим данные между процессами максимально поровну
    // Номера процессов 0..(separator-1) получат amount_process_data+1
    // Номера процессов separator..(NUM_THREADS-1) получат amount_process_data
    int amount_process = NUM_THREADS;
    int amount_process_data = 0;
    int separator;
    int tail;
    int num_data = (int)nPoints;
    do {
        tail = num_data % amount_process;
        amount_process_data += num_data / amount_process;
        num_data = tail;
    } while(tail >= amount_process);
    separator = tail;

    // double u[nPoints]; // Индексом является координата
    double *u = (double *)malloc(sizeof(double) * nPoints);
    double *u_new = (double *)malloc(sizeof(double) * nPoints);
    int i;

    double cur_t = 0;
    for (i = 0; i < nPoints; ++i) {
        u[i] = solution(x_coor(i));
    }

    for (cur_t = tau; cur_t <= T; cur_t += tau) {
        u_new[0] = 0;
        for (i = 1; i < nPoints; ++i) {
            u_new[i] = (-c/h * (u[i] - u[i-1])) * tau + u[i];
        }
        double *tmp = u;
        u = u_new;
        u_new = tmp;
    }

    for (i = 0; i < nPoints; ++i) {
        printf("%f ", u[i]);
    }

    // Exact solution
    printf("\n\n");
    for (i = 0; i < nPoints; ++i) {
        printf("%f ", exactSolution(x_coor(i), T));
    }


    /*


    pthread_t threads[NUM_THREADS];
    int rc, i;
    double l = left;

        sem_init(&sem, 0, 1);
        for (i = 0; i < NUM_THREADS; ++i) {
            if (i < separator) {
                num_data = amount_process_data + 1;
            } else {
                num_data = amount_process_data;
            }
            double *ar = (double *)malloc(sizeof(double) * 4);
            ar[0] = l;
            ar[1] = l + num_data*dx;
            ar[2] = dx;
            ar[3] = (double)i;
            l = ar[1];
            // printf("%f %f\n\n", ar[0], ar[1]);
            rc = pthread_create(&threads[i], NULL, CalcThread, (void *)ar);
            if (rc) {
                free(ar);
                printf("ERROR; return code from pthread_create() is %d\n", rc);
                exit(-1);
            }
        }

        for (i = 0; i < NUM_THREADS; ++i) {
            pthread_join(threads[i], NULL);
        }
        sem_destroy(&sem);

    printf("resultIntagrate = %f\n", resultIntagrate);
    */
}
