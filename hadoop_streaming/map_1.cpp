#include <iostream>
#include <sstream>
#include <string>

using std::string;
using std::istringstream;
using std::endl;
using std::cin;
using std::cout;

int main(int argc, char *argv[])
{
    string key;
    int a, b;
    int count = 0;
    while (std::getline(cin, key)) {
        count ++;
        if (key.empty()) {
            continue;
        }
        istringstream iss(key);
        iss >> a >> b;
        cout << a << "\t" << b << endl;
    }
    return 0;
}