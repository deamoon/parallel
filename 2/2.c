#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

double time_tot = 0.1;        // total time 
double l = 1;                 // length of the rod
double u_0 = 1;               // initial value
double pi = 3.14159265358;
int nPoints = 9;

int main(int argc, char *argv[]){
    int i, m, rank_process, amount_process;
    double time, x, sum, a;    
	double h = l / (nPoints + 1); // step on the x-coordinate
	double dt = time_tot / 20;  // time step
    
    double begin, end, total;

    MPI_Status Status;
    MPI_Init(&argc, &argv);
    begin = MPI_Wtime();
    MPI_Comm_size(MPI_COMM_WORLD, &amount_process);
    
    int sizePoint = nPoints; 

    // Разделим данные между процессами максимально поровну
    // Номера процессов 0..(separator-1) получат amount_process_data+1
    // Номера процессов separator..(amount_process-1) получат amount_process_data
    int amount_process_data = 0;
    int separator;
    int tail;
    int num_data = sizePoint;
    do {
        tail = num_data % amount_process;
        amount_process_data += num_data / amount_process;
        num_data = tail;
    } while(tail >= amount_process);
    separator = tail;

	MPI_Comm_rank(MPI_COMM_WORLD, &rank_process);

	if (rank_process < separator) {
		++amount_process_data;
	}	
		
	double *u_prev = (double *)(malloc(sizeof(double) * (amount_process_data+2)));
	double *u_next = (double *)(malloc(sizeof(double) * (amount_process_data+2)));
	
    for (i = 0; i < amount_process_data; i++){
        u_prev[i] = u_0;
        u_next[i] = u_0;    
    }      

    u_prev[0] = 0;
    u_prev[amount_process_data + 1] = 0;
    u_next[0] = 0;
    u_next[amount_process_data + 1] = 0;
    
    // main loop on time
    time = 0;
    while (time < time_tot){
        if (rank_process % 2 == 0) {
        	if (rank_process != amount_process-1)
        		MPI_Send(&u_prev[amount_process_data], 1, MPI_DOUBLE, rank_process+1, MSG_TAG, MPI_COMM_WORLD);
        	if (rank_process != 0) {
	        	MPI_Recv(&u_prev[0], 1, MPI_DOUBLE, rank_process-1, MSG_TAG, MPI_COMM_WORLD, &Status);
	        	MPI_Send(&u_prev[1], 1, MPI_DOUBLE, rank_process-1, MSG_TAG, MPI_COMM_WORLD);
	        }
        	if (rank_process != amount_process-1)
        		MPI_Recv(&u_prev[amount_process_data+1], 1, MPI_DOUBLE, rank_process+1, MSG_TAG, MPI_COMM_WORLD, &Status);
        } else {
        	if (rank_process != 0) 
        		MPI_Recv(&u_prev[0], 1, MPI_DOUBLE, rank_process-1, MSG_TAG, MPI_COMM_WORLD, &Status);
        	if (rank_process != amount_process-1) {
        		MPI_Send(&u_prev[amount_process_data], 1, MPI_DOUBLE, rank_process+1, MSG_TAG, MPI_COMM_WORLD);
        		MPI_Recv(&u_prev[amount_process_data+1], 1, MPI_DOUBLE, rank_process+1, MSG_TAG, MPI_COMM_WORLD, &Status);
        	}
        	if (rank_process != 0) 
        		MPI_Send(&u_prev[1], 1, MPI_DOUBLE, rank_process-1, MSG_TAG, MPI_COMM_WORLD);
        }
        for (i = 1; i <= amount_process_data; i+=2){
            u_next[i] = u_prev[i] + dt / (h * h) * (u_prev[i + 1] - 2 * u_prev[i] + u_prev[i - 1]);
        }

        for (i = 1; i <= amount_process_data; ++i){		
            u_prev[i] = u_next[i];
        }
        time = time + dt;
    }
    
    // printing the numerical solution on the screen 
    printf("Process # %d \n", rank_process);
    for (i = 1; i <= amount_process_data; i++){
        printf("%f  ", u_next[i]);
    }
    printf("\n");
    printf("\n");

    free(u_prev);
    free(u_next);
    
    end = MPI_Wtime();
    MPI_Finalize();
    total = end – begin;
    printf("Time = %f\n", total);

	///////////////////////////////////////////////////////////////////////////////////////////////

    double *u_exact = (double *)(malloc(sizeof(double) * (nPoints + 2)));
    // exact solution
    printf("Exact solution: \n");
    for (i = 1; i <= nPoints; i++){
        x = i * h;
        sum = 0;
    	for (m = 0; m < 5; m++){
        	a =  exp(- pi * pi * (2*m+1) * (2*m+1) * time_tot) * sin( pi * (2*m+1) * x / l) / (2*m+1);
            sum = sum + 4 * u_0 * a/ pi;
    	}
    	u_exact[i] = sum;
    	// printing the exact solution on the screen 
    	printf("%f  ", u_exact[i]);
    }
	printf("\n");

	free(u_exact);
	
    return 0;
}