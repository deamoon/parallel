#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

double func(double x) {
    return 1/x;
}

double resultIntagrate = 0;
const char * sem_name = "my_little_semaphore";
int NUM_THREADS = 5;

void *CalcSquare(void * arr) {
    double *ar = (double *)arr;
    // left = ar[0]; 
    // right = ar[1];
    // dx = ar[2];

    sem_t * s = sem_open(sem_name, O_CREAT, 0666, NUM_THREADS);
    double step;
    for (step = ar[0]; step < ar[1]; step += ar[2]) {
        sem_wait(s);
        resultIntagrate += (func(step) + func(step+ar[2]))/2 * ar[2];
        sem_post(s);
    }
    sem_close(s);
    free(ar);
    pthread_exit(NULL);
}

int main (int argc, char *argv[]) {
    double left = 0;
    double right = 1;
    double nPoints = 1000;
    double dx = (right - left) / nPoints;

    // Разделим данные между процессами максимально поровну
    // Номера процессов 0..(separator-1) получат amount_process_data+1
    // Номера процессов separator..(NUM_THREADS-1) получат amount_process_data
    int amount_process = NUM_THREADS;
    int amount_process_data = 0;
    int separator;
    int tail;
    int num_data = nPoints;
    do {
        tail = num_data % amount_process;
        amount_process_data += num_data / amount_process;
        num_data = tail;
    } while(tail >= amount_process);
    separator = tail;

    pthread_t threads[NUM_THREADS];
    int rc, i, num_data
    int l = left;

    // sem_t * s = sem_open(sem_name, O_CREAT, 0666, NUM_THREADS);
    for (i = 0; i < NUM_THREADS; ++i) {
        if (i < separator) {
            num_data = amount_process_data + 1;
        } else {
            num_data = amount_process_data;
        }
        double *ar = (double *)malloc(sizeof(double) * 3);
        ar[0] = l;
        ar[1] = l + num_data*dx;
        ar[2] = dx;

        rc = pthread_create(&threads[i], NULL, CalcSquare, (void *)ar);
        if (rc) {
            free(ar);
            printf("ERROR; return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
        
        l = ar[1];
    }
    
    for (i = 0; i < NUM_THREADS; ++i) {
        pthread_join(threads[i], NULL);
    }
    // sem_close(s);
    sem_unlink(sem_name);

    printf("%f\n", resultIntagrate);
}
