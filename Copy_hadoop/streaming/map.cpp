/* 
hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
-input /data/matrix/A/matrix_5000x20000.cvs \
-output /user/s29412/super_out/out14 \
-mapper ./map \
-reducer ./reduce.py \
-file /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
-file map \
-file reduce.py \
-numReduceTasks 3
*/

// -input /data/matrix/A/matrix_5000x20000.cvs \
// -input /data/matrix/C/matrix_20000x50.csv \

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

bool check_file(const string &file) {
    for (int i = file.length(); i >= 0; --i) {
        if (file[i] == 'A') {
            return true;
        }
        if (file[i] == 'C') {
            return false;
        }
    }
    return false;
}

int main(int argc, char *argv[]) {
    int num;
    int step = 0;
    int i, j, k;    
    int matrix;
    bool eof, is_A;
    char* pPath;
    string file;

    // A = 5.000  x 20.000 (m x n)
    // B = 20.000 x 50 (n x p)
    int m = 5000;
    int n = 20000;
    int p = 50;
    // int m = 2;
    // int n = 3;
    // int p = 2;
    
    step = 0;
    while (1) {
        // pPath = getenv("map_input_file");
        // if (pPath) {
        //     file = string(pPath);    
        // } else {
        //     file = "";
        // }        
        eof = (cin >> num);
        if (!eof) {
            break;
        }

        // is_A = check_file(file);
        is_A = true;
        if (is_A) {
            if (step == 0) {
                i = num;
                ++step;
                if (cin.peek() == ',') {
                    cin.ignore();
                }
            } else if (step == 1) {
                j = num;
                ++step;
            } else {
                // for (k = 0; k < p; ++k) {
                //     cout << i << ',' << k << '\t' << "A," << j << ',' << num << endl;
                // }   
                cout << i << ',' << '\t' << "A," << j << ',' << num << endl;         
                step = 0;
            }
        } else {
            if (step == 0) {
                j = num;
                ++step;
                if (cin.peek() == ',') {
                    cin.ignore();
                }
            } else if (step == 1) {
                k = num;
                ++step;
            } else {
                // for (i = 0; i < m; ++i) {
                //     cout << i << ',' << k << '\t' << "B," << j << ',' << num << endl;
                // }            
                cout << k << '\t' << "B," << j << ',' << num << endl;
                step = 0;
            }
        }


    }


    return 0;
}