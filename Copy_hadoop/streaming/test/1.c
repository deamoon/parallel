#include <stdio.h>
#include <stdlib.h>

int main() {
	int a, b, c;
	char separator;
	while (!feof(stdin)) {		
		if (scanf( "%d,%d", &a, &b) != 2) {
			break;
		}
		
		separator = getchar();
		if (!separator) {
			break;		
		}

		if (scanf("%d", &c) != 1) {
			break;
		}

		if (separator == ',') {
			printf("A,%d,%d,%d", a, b, c);
		} else {
			printf("B,%d,%d,%d", a, b, c);
		}
	}
	return 0;
}