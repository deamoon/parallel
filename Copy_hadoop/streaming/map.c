/* 
hadoop jar /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
-input /data/matrix/A/matrix_5000x20000.cvs \
-output /user/s29412/super_out2/out4 \
-mapper ./map \
-reducer ./reduce \
-file /usr/lib/hadoop-mapreduce/hadoop-streaming.jar \
-file map \
-file reduce \
-file matrix_20000x50.csv \
-numReduceTasks 1
*/

// -input /data/matrix/A/matrix_5000x20000.cvs \
// -input /data/matrix/C/matrix_20000x50.csv \


#include <stdio.h>
#include <stdlib.h>

int main() {
	int i, j, num, k;	
    // int m = 5000;
    // int n = 20000;
    // int p = 50;
	char separator;
	
	while (!feof(stdin)) {		
		if (scanf( "%d,%d", &i, &j) != 2) {
			break;
		}
		
		separator = getchar();
		if (!separator) {
			break;		
		}

		if (scanf("%d", &num) != 1) {
			break;
		}

		printf("%d\t%d,%d\n", i, j, num);
	}
	return 0;
}