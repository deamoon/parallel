#include <stdio.h>
#include <stdlib.h>

int main() {
    // int m = 5000;
    int n = 20000;
    int p = 50;
    // int n = 3;
    // int p = 2;
    int matrix_B[n][p];
    int row_A[n];
	int i, j, num;
	long long int result;
	FILE *file;
	file = fopen("matrix_20000x50.csv", "r");
	if (file) {
    	while (!feof(file) && fscanf(file, "%d,%d\t%d", &i, &j, &num) == 3) {
        	matrix_B[i][j] = num;	
    	}
    	fclose(file);
	}

	int key = -1;
	int current_key = -1;
	while (!feof(stdin) && scanf("%d\t%d,%d\n", &key, &j, &num) == 3) {	
		if (current_key != -1 && current_key != key) {
			for (int col = 0; col < p; ++col) {
				result = 0;
				for (int t = 0; t < n; ++t) {
					result += row_A[t] * matrix_B[t][col];
				}
				printf("%d,%d\t%lld\n", current_key, col, result);
			}
			current_key = key;
		}
		if (current_key == -1) {
			current_key = key;
		}
		row_A[j] = num;
	}
	if (current_key != -1) {
		for (int col = 0; col < p; ++col) {
			result = 0;
			for (int t = 0; t < n; ++t) {
				result += row_A[t] * matrix_B[t][col];
			}
			printf("%d,%d\t%lld\n", current_key, col, result);
		}
	}

    
	return 0;
}