#!/usr/bin/env python

import sys

# ./map | sort -k1,1 | ./reduce.py

def main():
    matrix_B = {}
    with open("matrix_20000x50.csv", 'r') as f:
        for line in f.readlines():
            line = line.strip()
            key, value = line.split('\t', 1)
            i, j = key.split(',', 1)
            if not i in matrix_B:
                matrix_B[i] = {} 
            
            try:
                value = int(value)
            except ValueError:        
                continue   
            
            matrix_B[i][j] = value                
    
    number_col_B = len(matrix_B['0'].keys())
    current_key = None
    result = 0
    key = None
    row_A = {}
    for line in sys.stdin:
        line = line.strip()
        key, value = line.split('\t', 1)        
        
        try:
            key = int(key)
        except ValueError: 
            print key       
            continue

        if not current_key is None and current_key != key:                        
            for k in range(number_col_B):
                result = 0
                col = str(k)
                for j in row_A:
                    result += row_A[j] * matrix_B[j][col]
                print '%s,%s\t%s' % (current_key, col, result)            
            row_A = {}            
            current_key = key

        if current_key is None:
            current_key = key

        j, num = value.split(',')
        
        try:
            num = int(num)
        except ValueError:    
            print num   
            continue
        
        row_A[j] = num
    

    if current_key:
        for k in range(number_col_B):
            result = 0
            col = str(k)
            for j in row_A:
                result += row_A[j] * matrix_B[j][col]
            print '%s,%s\t%s' % (current_key, col, result)  

if __name__ == '__main__':
    main()