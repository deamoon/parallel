#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

double func(double x) {
    return 1/(1+x*x);
}

double resultIntagrate = 0;
sem_t sem;

void *CalcSquare(void * arr) {
    double *ar = (double *)arr;
    // left = ar[0];
    // right = ar[1];
    // dx = ar[2];

    printf("Thread # %d\n", (int)ar[3]);

    double step;
    double threadResult = 0;
    for (step = ar[0]; step < ar[1]; step += ar[2]) {
        threadResult += (func(step) + func(step+ar[2]))/2 * ar[2];
    }
    //free(ar);

    //sem_wait(&sem);
        resultIntagrate += threadResult;
    //sem_post(&sem);

    //pthread_exit(NULL);
}

int main (int argc, char *argv[]) {
    double left = 0;
    double right = 1;
    double nPoints = 100000000;
    int NUM_THREADS = 0;
    double dx = (right - left) / nPoints;

    // Разделим данные между процессами максимально поровну
    // Номера процессов 0..(separator-1) получат amount_process_data+1
    // Номера процессов separator..(NUM_THREADS-1) получат amount_process_data
    /*int amount_process = NUM_THREADS;
    int amount_process_data = 0;
    int separator;
    int tail;
    int num_data = nPoints;
    do {
        tail = num_data % amount_process;
        amount_process_data += num_data / amount_process;
        num_data = tail;
    } while(tail >= amount_process);
    separator = tail;

    pthread_t threads[NUM_THREADS];
    int rc, i;
    double l = left;
*/
int i;
    float time;
    clock_t init, t;
    init = clock();

        /*sem_init(&sem, 0, 1);
        for (i = 0; i < NUM_THREADS; ++i) {
            if (i < separator) {
                num_data = amount_process_data + 1;
            } else {
                num_data = amount_process_data;
            }
            double *ar = (double *)malloc(sizeof(double) * 4);
            ar[0] = l;
            ar[1] = l + num_data*dx;
            ar[2] = dx;
            ar[3] = (double)i;
            l = ar[1];
            // printf("%f %f\n\n", ar[0], ar[1]);
            rc = pthread_create(&threads[i], NULL, CalcSquare, (void *)ar);
            if (rc) {
                free(ar);
                printf("ERROR; return code from pthread_create() is %d\n", rc);
                exit(-1);
            }
        }

        for (i = 0; i < NUM_THREADS; ++i) {
            pthread_join(threads[i], NULL);
        }
        sem_destroy(&sem);*/

    double *ar = (double *)malloc(sizeof(double) * 4);
    ar[0] = left;
    ar[1] = right;
    ar[2] = dx;
    ar[3] = (double)i;

    CalcSquare((void *)ar);
    free(ar);
    // printf("%f %f\n\n", ar[0], ar[1]);
    //rc = pthread_create(&threads[i], NULL, CalcSquare, (void *)ar);

    t = clock() - init;
    time= (float) t / CLOCKS_PER_SEC;
    printf("Сlicks %d, time = %f sec\n", t, time);

    printf("resultIntagrate = %f\n", resultIntagrate);
    printf("resultIntagrate = %f\n", resultIntagrate * 4);
}
